﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	[Header("This overrides player input.")]
	public bool enableAI;
	public GameObject puck;
	public bool useKeyboard;
	public float speed = 10f;
	public GameObject AISpawn;

	public string horizontalAxis;
	public string verticalAxis;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;

		if(enableAI)
		{
			StartCoroutine (AICheck ());
	}
}
	
	// Update is called once per frame
	//void Update () {
		//Debug.Log ("Time = " + Time.time);
		
	//}

	void FixedUpdate(){
	if(!enableAI)
	{
			if (useKeyboard) {
				Vector3 pos = new Vector3 (Input.GetAxis (horizontalAxis), 0, Input.GetAxis (verticalAxis));
				rigidbody.AddForce (speed * pos);
			}
		else{
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
				Vector3 vel = dir.normalized * speed;
		//Vector3 vel = dir.normalized * speed;

		//check if this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;
		if (move > distToTarget) {
			//scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}
		rigidbody.velocity = vel;
	}
	}
}
	IEnumerator AICheck()
	{
		rigidbody.velocity = Vector3.zero;
		Vector3 pos = new Vector3(puck.transform.localPosition.x, 0, puck.transform.localPosition.z);
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget)
		{
			vel = vel * distToTarget / move;
		}
		rigidbody.velocity = vel;
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(AICheck());
	}

	private Vector3 GetMousePosition() {
		//create a ray from the camera
		//passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		//find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}

	public void ResetAIPosition(){
		transform.position = AISpawn.transform.position;
	}

	void OnDrawGizmos() {
		//draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
}
