﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;

	public int player; //who gets points for scoring goals

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider){
		//play score sound
		audio.PlayOneShot(scoreClip);


		Scorekeeper.Instance.ScoreGoal (player); //tell the scorekeeper

		//reset the puck to its starting position
		PuckControl puck = 
			collider.gameObject.GetComponent<PuckControl>();
		puck.ResetPosition ();

	}
	// Update is called once per frame
	void Update () {
		
	}
}
